
declare const pugbingo_host: string;
declare const pugbingo_port: string;
declare const pugbingo_prefix: string;

export function getRoomInfoAPIString(id: string): string {
    return pugbingo_host + ":" + pugbingo_port + "" + pugbingo_prefix + "/rooms/" + id + "/info";
}

export function getRoomAPIString(id: string): string {
    return pugbingo_host + ":" + pugbingo_port + "" + pugbingo_prefix + "/rooms/" + id;
}

export function postJoinRoomAPIString(roomId: string, userId: string): string {
    return pugbingo_host + ":" + pugbingo_port + "" + pugbingo_prefix + "/rooms/" + roomId + "/user/" + userId;
}

export function deleteLeaveRoomAPIString(roomId: string, userId: string): string {
    return pugbingo_host + ":" + pugbingo_port + "" + pugbingo_prefix + "/rooms/" + roomId + "/user/" + userId;
}

export function getAllRoomsAPIString(): string {
    return pugbingo_host + ":" + pugbingo_port + "" + pugbingo_prefix + "/rooms/";
}

export function getPostRoomAPIString(): string {
    return pugbingo_host + ":" + pugbingo_port + "" + pugbingo_prefix + "/rooms/";
}

export function getLogInUserAPIString(username: string): string {
    return pugbingo_host + ":" + pugbingo_port + "" + pugbingo_prefix + "/user/" + username;
}

export function getPostCallTermAPIString(roomId: string): string {
    return pugbingo_host + ":" + pugbingo_port + "" + pugbingo_prefix + "/rooms/" + roomId + "/terms";
}

export function getPostUserAPIString(): string {
    return pugbingo_host + ":" + pugbingo_port + "" + pugbingo_prefix + "/user";
}

export function getDeleteRoomAPIString(roomId: string) {
    return pugbingo_host + ":" + pugbingo_port + "" + pugbingo_prefix + "/rooms/" + roomId;
}

export function getAllTemplateIdsString(): string {
    return pugbingo_host + ":" + pugbingo_port + "" + pugbingo_prefix + "/templates";
}

export function getTemplateString(id: string): string {
    return pugbingo_host + ":" + pugbingo_port + "" + pugbingo_prefix + "/templates/" + id;
}

export function postNewTemplateString(): string {
    return pugbingo_host + ":" + pugbingo_port + "" + pugbingo_prefix + "/templates/";
}