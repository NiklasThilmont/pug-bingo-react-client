import * as React from "react";
import "./App.css";

import {BingoApp} from "./bingo/bingo-app_module";


class App extends React.Component<{}, null> {
  render() {
      // <BingoRoomList width={200}/>
    return (

            <BingoApp/>

    );
  }
}

export default App;
