import {connect} from "react-redux";
import * as React from "react";
import * as redux from "redux";
import {ApplicationState} from "../../index";
import {RaisedButton, TextField} from "material-ui";
import {BingoUser} from "../../model/BingoUser";
import {isNullOrUndefined} from "util";
import {ActionCreator} from "../../store/ActionCreator";

/**
 * Properties that are managed by react-redux.
 *
 * Each property defined here will also need a corresponding mapping in the {@link AccountInfo.mapStateToProps} method,
 * otherwise the component will not render and update correctly.
 */
interface AccountInfoProps {
    user: BingoUser;
}

/**
 * Local properties of this module.
 *
 * These properties are used to initialize the local state and to control everything that is solely used for the display layer.
 *
 * Data that is intended not only for display needs to be placed in the {@link AccountInfoProps} and will then be
 * managed by redux.
 */
interface AccountInfoLocalProps {

}

/**
 * Local state of the module.
 *
 * All display-only state fields, such as bool flags that define if an element is visibile or not, belong here.
 */
interface AccountInfoLocalState {
    username: string;
}

/**
 * Defines mappings from local handlers to redux dispatches that invoke actions on the store.
 */
interface AccountInfoDispatch {
    createOrLogInUser(username: string): void;
}

class AccountInfoModule extends React.Component<
    AccountInfoProps
    & AccountInfoLocalProps
    & AccountInfoDispatch, AccountInfoLocalState> {

    constructor(props: AccountInfoProps & AccountInfoLocalProps & AccountInfoDispatch) {
        super(props);
        this.state = {
            username: ""
        };

    }

    static mapStateToProps(state: ApplicationState, localProps: AccountInfoLocalProps): AccountInfoProps {
        return {
            user: state.bingoStore.user()
        }
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<ApplicationState>): AccountInfoDispatch {
        return {
            createOrLogInUser: (username) => {dispatch(ActionCreator.AsyncCreateOrLogIn(username))}
        }
    }

    private changeUserName = (ignore: any, value: string) => {
        this.setState({
            username: value
        })
    };

    private createOrLogInUser =() => {
        this.props.createOrLogInUser(this.state.username);
    };

    render() {
        if(!isNullOrUndefined(this.props.user)) {
            return (
                <div style={{marginLeft: "30px"}}>
                    <TextField
                        floatingLabelText="Display Name"
                        value={this.props.user.name()}
                        onChange={this.changeUserName}
                        disabled={true}
                    />
                    <br/>
                    <TextField
                        floatingLabelText="GW2 API Key"
                        disabled={true}
                    />
                    <br/>
                    <RaisedButton
                        label="Save(Not Yet Implemeted)"
                        primary={true}
                        disabled={true}
                    />
                </div>);
        }
        return (<div style={{marginLeft: "30px", paddingLeft: "50px"}}>
                    <TextField
                        floatingLabelText="Username"
                        value={this.state.username}
                        onChange={this.changeUserName}
                    />
                    <br/>
                    <RaisedButton
                        label="Create or Log In"
                        onClick={this.createOrLogInUser}
                        primary={true}
                    />
                </div>
        )

    }
}

/**
 * @see AccountInfoModule
 * @author nt
 * @since 26.05.2017
 */
export const AccountInfo: React.ComponentClass<AccountInfoLocalProps> = connect(AccountInfoModule.mapStateToProps, AccountInfoModule.mapDispatchToProps)(AccountInfoModule);