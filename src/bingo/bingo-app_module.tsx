import {connect} from "react-redux";
import * as React from "react";
import * as redux from "redux";
import {ApplicationState} from "../index";
import {
    FlatButton, FontIcon, RaisedButton, Snackbar, Toolbar, ToolbarGroup, ToolbarSeparator,
    ToolbarTitle
} from "material-ui";
import {BingoRoomList} from "./room/bingo-room-list_module";
import {SingleBingoRoom} from "./room/single-bingo-room_module";
import {AccountInfo} from "./account/account-info_module";
import {RouteTarget} from "../model/RouteTarget";
import {ActionCreator} from "../store/ActionCreator";
import {TemplateList} from "./template/template-list_module";
import {BingoUser} from "../model/BingoUser";
import {isNullOrUndefined} from "util";


/**
 * Properties that are managed by react-redux.
 *
 * Each property defined here will also need a corresponding mapping in the {@link BingoApp.mapStateToProps} method,
 * otherwise the component will not render and update correctly.
 */
interface BingoAppProps  {
    currentTarget: RouteTarget;
    user: BingoUser;
}

/**
 * Local properties of this module.
 *
 * These properties are used to initialize the local state and to control everything that is solely used for the display layer.
 *
 * Data that is intended not only for display needs to be placed in the {@link BingoAppProps} and will then be
 * managed by redux.
 */
interface BingoAppLocalProps{

}

/**
 * Local state of the module.
 *
 * All display-only state fields, such as bool flags that define if an element is visibile or not, belong here.
 */
interface BingoAppLocalState {
    popoverOpen: boolean;
}

/**
 * Defines mappings from local handlers to redux dispatches that invoke actions on the store.
 */
interface BingoAppDispatch  {
    changeRoute(to: RouteTarget): void;
    logOutUser(): void;
}

class BingoAppModule extends React.Component<BingoAppProps & BingoAppLocalProps & BingoAppDispatch, BingoAppLocalState> {

    private bingoRoomList = <BingoRoomList/>;
    private bingoRoom = <SingleBingoRoom/>;
    private accountInfo = <AccountInfo/>;
    private templates = <TemplateList/>;

    constructor(props: BingoAppProps & BingoAppLocalProps & BingoAppDispatch) {
        super(props);
        this.state = {
            popoverOpen: false,
        };
    }

    static mapStateToProps(state: ApplicationState, localProps: BingoAppLocalProps): BingoAppProps {
        return {
            currentTarget: state.bingoStore.routeTarget(),
            user: state.bingoStore.user()
        }
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<ApplicationState>): BingoAppDispatch {
        return {
            changeRoute: (to) => dispatch(ActionCreator.ChangeRoute(to)),
            logOutUser: () =>  dispatch(ActionCreator.AsyncLogOutUser())
        }
    }

    public componentWillReceiveProps(nextProps: BingoAppProps & BingoAppLocalProps & BingoAppDispatch) {
        console.log(nextProps);
    }


    private showPopver = () => {
        this.setState({
            popoverOpen: true
        })
    };


    private renderCurrent = () => {
        switch(this.props.currentTarget) {
            case RouteTarget.Account:
                return this.accountInfo;
            case RouteTarget.Room:
                return this.bingoRoom;
            case RouteTarget.RoomList:
                return this.bingoRoomList;
            case RouteTarget.Templates:
                return this.templates;
        }
        return null;
    };

    private handleRouteToTemplates= () => {
        this.props.changeRoute(RouteTarget.Templates)
    };

    render() {
        return (<div>
            <Toolbar>
                <ToolbarGroup>
                    <ToolbarTitle text="PUG-Bingo 2.0" />
                    <ToolbarSeparator />
                    <RaisedButton
                        label="Rooms"
                        primary={true}
                        icon={<FontIcon className="material-icons">list</FontIcon>}
                        onClick={() => this.props.changeRoute(RouteTarget.RoomList)}
                    />
                    <RaisedButton
                        label="Current Room"
                        primary={true}
                        icon={<FontIcon className="material-icons">room</FontIcon>}
                        onClick={() => this.props.changeRoute(RouteTarget.Room)}
                    />
                    <RaisedButton
                        label="Templates"
                        primary={true}
                        icon={<FontIcon className="material-icons">view_list</FontIcon>}
                        onClick={this.handleRouteToTemplates}
                    />
                </ToolbarGroup>
                <ToolbarGroup lastChild={true}>
                    <RaisedButton
                        label="My Account"
                        onClick={() => this.props.changeRoute(RouteTarget.Account)}
                    />
                    {
                        !isNullOrUndefined(this.props.user)
                        ?
                            <FlatButton
                                label="Logout"
                                onClick={this.props.logOutUser}
                            />
                        :
                            null
                    }

                </ToolbarGroup>
            </Toolbar>
            <Snackbar
                open={isNullOrUndefined(this.props.user)}
                message="No account set. Please go to My Account and set one"
            />
            {this.renderCurrent()}
        </div>);
    }
}

/**
 * @see BingoAppModule
 * @author nt
 * @since 26.05.2017
 */
export const BingoApp: React.ComponentClass<BingoAppLocalProps> = connect(BingoAppModule.mapStateToProps, BingoAppModule.mapDispatchToProps)(BingoAppModule);