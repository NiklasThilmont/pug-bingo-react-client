import {connect} from "react-redux";
import * as React from "react";
import * as redux from "redux";
import {ApplicationState} from "../../index";
import {Dialog, Divider, FontIcon, IconButton, List, ListItem, RaisedButton, TextField} from "material-ui";
import * as Immutable from "immutable";
import {BingoRoom} from "../../model/BingoRoom";
import {ActionCreator} from "../../store/ActionCreator";
import {BingoRoomCreationDialog} from "./bingo-room-creation_module";
import {BingoRoomInfo} from "../../model/BingoRoomInfo";
import {RoomEnterStatus} from "../../model/RoomEnterStatus";
import {BingoUser} from "../../model/BingoUser";
import { isNullOrUndefined} from "util";
import {BingoTemplate} from "../../model/BingoTemplate";

/**
 * Properties that are managed by react-redux.
 *
 * Each property defined here will also need a corresponding mapping in the {@link BingoRoomList.mapStateToProps} method,
 * otherwise the component will not render and update correctly.
 */
interface BingoRoomListProps {
    roomInfos: Immutable.Map<string, BingoRoomInfo>
    roomEnterStatus: RoomEnterStatus;
    user: BingoUser;
    templates: Immutable.List<BingoTemplate>;
}

/**
 * Local properties of this module.
 *
 * These properties are used to initialize the local state and to control everything that is solely used for the display layer.
 *
 * Data that is intended not only for display needs to be placed in the {@link BingoRoomListProps} and will then be
 * managed by redux.
 */
interface BingoRoomListLocalProps {
    width?: number;
}

/**
 * Local state of the module.
 *
 * All display-only state fields, such as bool flags that define if an element is visibile or not, belong here.
 */
interface BingoRoomListLocalState {
    dialogOpen: boolean;
    selectedRoomId: string;
    roomPass: string;
}

/**
 * Defines mappings from local handlers to redux dispatches that invoke actions on the store.
 */
interface BingoRoomListDispatch {
    addRoom(room: BingoRoom): void;
    showPasswordDialog(): void;
    closePasswordDialog(): void;
    getAllRooms(): void;
    joinRoom(roomId: string, userId: string, password?: string): void;
    getAllTemplates(): void;
    createTemplate(terms: Array<string>, name: string): void;
}

class BingoRoomListModule extends React.Component<BingoRoomListProps & BingoRoomListLocalProps & BingoRoomListDispatch, BingoRoomListLocalState> {

    constructor(props: BingoRoomListProps & BingoRoomListLocalProps & BingoRoomListDispatch) {
        super(props);
        this.state = {
            dialogOpen: false,
            selectedRoomId: null,
            roomPass: ""
        }
    }

    static mapStateToProps(state: ApplicationState, localProps: BingoRoomListProps): BingoRoomListProps {
        return {
            roomInfos: state.bingoStore.roomInfos(),
            roomEnterStatus: state.bingoStore.roomEnterStatus(),
            user: state.bingoStore.user(),
            templates: state.bingoStore.templates()
        }
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<ApplicationState>): BingoRoomListDispatch {
        return {
            addRoom: (room: BingoRoom) => {dispatch(ActionCreator.AsyncCreateRoom(room))},
            getAllRooms: () => dispatch(ActionCreator.AsyncReloadMissingRooms()),
            showPasswordDialog: () => dispatch(ActionCreator.RequestJoinRoom()),
            joinRoom:(id, userId, password?) => dispatch(ActionCreator.AsyncJoinRoom(id, userId, password)),
            closePasswordDialog: () => dispatch(ActionCreator.StopJoinRoom()),
            getAllTemplates: () => dispatch(ActionCreator.AsyncLoadAllTemplates()),
            createTemplate: (terms, name) => dispatch(ActionCreator.AsyncSaveTemplate(terms, name))
        }
    }

    private handleAddButtonPress = () => {
        this.props.getAllTemplates();
        this.setState({
            dialogOpen: true
        })
    };

    private createRoom = (room: BingoRoom) => {
        this.setState({
            dialogOpen: false
        });
        this.props.addRoom(room);
    };

    private handleUserPassInput = (evt: any, value: string) => {
        this.setState({
            roomPass: value
        })
    };

    private handlePasswordOkButton = () => {
        this.props.joinRoom(this.state.selectedRoomId, this.props.user.id(), this.state.roomPass);
    };

    private handleSelectRoom = (id: string) => {
        if(this.props.roomInfos.get(id).hasPassword()) {
            this.setState({
                selectedRoomId: id
            });
            this.props.showPasswordDialog();
        } else {
            this.props.joinRoom(id, this.props.user.id());
        }
    };

    private getPassErrorText = () => {
        if(this.props.roomEnterStatus == RoomEnterStatus.PasswordReject) {
            return "Invalid password";
        }
        return null;
    };

    private renderRooms = () => {
        let res: Array<JSX.Element> = [];
        this.props.roomInfos.forEach((value: BingoRoomInfo, key: string) => {
            res.push(
                    <ListItem
                        key={key}
                    >
                        <div className="row">
                            <div className="col-md-3">
                                {value.roomName()}
                            </div>
                            <div className="col-md-1">
                                {value.hasPassword() ? <FontIcon className="material-icons">lock</FontIcon> : null}
                            </div>
                            <div className="col-md-3">
                                <RaisedButton
                                    label="Join"
                                    onClick={() => this.handleSelectRoom(value.id())}
                                    disabled={isNullOrUndefined(this.props.user)}
                                />
                            </div>
                        </div>
                    </ListItem>
            );
        });
        return res;
    };


    render() {
        return (
        <div style={{width: this.props.width}}>
            <BingoRoomCreationDialog
                open={this.state.dialogOpen}
                onCreate={this.createRoom}
                onRequestClose={() => this.setState({dialogOpen: false})}
                templates={this.props.templates}
                onCreateTemplate={this.props.createTemplate}
            />
            <Dialog
                open={this.props.roomEnterStatus != RoomEnterStatus.None}
                title="Room Password Required"
                onRequestClose={this.props.closePasswordDialog}
            >
                <TextField
                    floatingLabelText="Password"
                    value={this.state.roomPass}
                    onChange={this.handleUserPassInput}
                    errorText={this.getPassErrorText()}
                />
                <br/>
                <RaisedButton
                    label="OK"
                    onClick={this.handlePasswordOkButton}
                />
                <RaisedButton
                    label="Stop"
                    secondary={true}
                    onClick={this.props.closePasswordDialog}
                />
            </Dialog>
            <IconButton
                iconClassName="material-icons"
                tooltip="Refresh"
                onClick={this.props.getAllRooms}
            >
                update
            </IconButton>
            <List>
                {this.renderRooms()}
                <Divider/>
                <ListItem> <RaisedButton primary={true} label="New room" onClick={this.handleAddButtonPress} /></ListItem>
            </List>
        </div>);
    }
}

/**
 * @see BingoRoomListModule
 * @author Niklas
 * @since 25.05.2017
 */
export const BingoRoomList: React.ComponentClass<BingoRoomListLocalProps> = connect(BingoRoomListModule.mapStateToProps, BingoRoomListModule.mapDispatchToProps)(BingoRoomListModule);