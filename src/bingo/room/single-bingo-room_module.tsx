import {connect} from "react-redux";
import * as React from "react";
import * as redux from "redux";
import {ApplicationState} from "../../index";
import {BingoRoom} from "../../model/BingoRoom";
import {AutoComplete, List, ListItem, Paper, RaisedButton, Subheader} from "material-ui";
import {isNullOrUndefined} from "util";
import {ActionCreator} from "../../store/ActionCreator";
import {BingoGameField} from "../bingofield/bingo_module";
import {BingoBoard} from "../../model/BingoBoard";
import {BingoParticipant} from "../../model/BingoParticipant";
import {BingoUser} from "../../model/BingoUser";

/**
 * Properties that are managed by react-redux.
 *
 * Each property defined here will also need a corresponding mapping in the {@link SingleBingoRoom.mapStateToProps} method,
 * otherwise the component will not render and update correctly.
 */
interface SingleBingoRoomProps {
    room: BingoRoom;
    board: BingoBoard;
    user: BingoUser
}

/**
 * Local properties of this module.
 *
 * These properties are used to initialize the local state and to control everything that is solely used for the display layer.
 *
 * Data that is intended not only for display needs to be placed in the {@link SingleBingoRoomProps} and will then be
 * managed by redux.
 */
interface SingleBingoRoomLocalProps {
}

/**
 * Local state of the module.
 *
 * All display-only state fields, such as bool flags that define if an element is visibile or not, belong here.
 */
interface SingleBingoRoomLocalState {
    termSearchText: string;
}

/**
 * Defines mappings from local handlers to redux dispatches that invoke actions on the store.
 */
interface SingleBingoRoomDispatch {
    callTerm(room: BingoRoom, term: string): void;
    deleteRoom(roomId: string, password?: string): void;
    leaveRoom(roomId: string, userId: string, roomPass?: string): void;

}

class SingleBingoRoomModule extends React.Component<SingleBingoRoomProps & SingleBingoRoomLocalProps & SingleBingoRoomDispatch, SingleBingoRoomLocalState> {

    constructor(props: SingleBingoRoomProps & SingleBingoRoomLocalProps & SingleBingoRoomDispatch) {
        super(props);
        this.state = {
            termSearchText: ""
        }
    }

    static mapStateToProps(state: ApplicationState, localProps: SingleBingoRoomLocalProps): SingleBingoRoomProps {
        let board: BingoBoard = null;
        if(!isNullOrUndefined(state.bingoStore.currentRoom())) {
            state.bingoStore.currentRoom().participants().forEach((value, key, iter) => {
                if(value.userId() == state.bingoStore.user().id()) board = value.bingoBoard();
            });
        }
        return {
            room: state.bingoStore.currentRoom(),
            board: board,
            user: state.bingoStore.user()
        }
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<ApplicationState>): SingleBingoRoomDispatch {
        return {
            callTerm: ((room, term) => {
                dispatch(ActionCreator.AsyncCallTerm(room.id(), term, room.password()))
            }),
            deleteRoom: (roomId, password?) => {
                dispatch(ActionCreator.AsyncDeleteRoom(roomId, password))
            },
            leaveRoom: (roomId, userId, roomPass) => dispatch(ActionCreator.AsyncLeaveRoom(roomId, userId, roomPass))
        }
    }

    private updateTermSearchText = (value:string) => {
        this.setState({
            termSearchText: value
        })
    };

    private callTerm = () => {
        this.props.callTerm(this.props.room, this.state.termSearchText);
    };

    private deleteRoom = () => {
        this.props.deleteRoom(this.props.room.id(), this.props.room.password());
    };

    private leaveRoom = () => {
        this.props.leaveRoom(this.props.room.id(), this.props.user.id(), this.props.room.password())
    };

    private getWinner = (id: string) => {
        let winner: BingoParticipant = null;
        this.props.room.participants().forEach(p => {
            if(p.userId() == id) {
                winner = p;
            }
        });
        return winner;
    };

    private renderParticipants = () => {
        return this.props.room.participants().map(p => {
            return (<ListItem key={"BingoRoom.Participant." + p.userId()}>{p.userName()}</ListItem>)
        })
    };


    private renderWinners = () => {
        return this.props.room.winnerIds().map(winner => {
            let p = this.getWinner(winner);
            if(!isNullOrUndefined(p)) {
                return (<ListItem key={"BingoRoom.Participant." + p.userId()}>{p.userName()}</ListItem>)
            }
            return null;
        })
    };

    render() {
        if(!isNullOrUndefined(this.props.room)) {
            return (
                <Paper>
                    <div className="row">
                        <div className="col-md-6">
                            <BingoGameField field={this.props.board} />
                        </div>
                        <div className="col-md-3">
                            <AutoComplete
                                floatingLabelText="Callable Terms"
                                dataSource={this.props.room.terms().toArray()}
                                onUpdateInput={this.updateTermSearchText}
                                onNewRequest={this.updateTermSearchText}
                                searchText={this.state.termSearchText}
                                filter={AutoComplete.fuzzyFilter}
                            />
                            <RaisedButton label="Call Term" onClick={this.callTerm}/>
                        </div>
                        <div className="col-md-3">
                            <List>
                                <Subheader>Participants</Subheader>
                                {
                                    this.renderParticipants()
                                }
                            </List>
                            <List>
                                <Subheader>Winners</Subheader>
                                {
                                    this.renderWinners()
                                }
                            </List>
                            <RaisedButton secondary={true} label="Delete Room" onClick={this.deleteRoom}/>
                            <RaisedButton label="Leave Room" onClick={this.leaveRoom}/>
                        </div>
                    </div>
                </Paper>
            );
        }
        return (<h1>No Room Selected</h1>)
    }
}

/**
 * @see SingleBingoRoomModule
 * @author Niklas
 * @since 25.05.2017
 */
export const SingleBingoRoom: React.ComponentClass<SingleBingoRoomLocalProps> = connect(SingleBingoRoomModule.mapStateToProps, SingleBingoRoomModule.mapDispatchToProps)(SingleBingoRoomModule);