import * as React from "react";
import {
    Dialog, GridTile, Step, TextField, MenuItem, SelectField, GridList, RaisedButton,
    AutoComplete, Checkbox
} from "material-ui";
import {BingoRoom} from "../../model/BingoRoom";
import * as Immutable from "immutable";
import {Stepper, StepLabel} from "material-ui/Stepper";
import {BingoTemplate} from "../../model/BingoTemplate";

interface DialogProps {
    open: boolean;
    templates: Immutable.List<BingoTemplate>;
    onCreate(room: BingoRoom): void;
    onCreateTemplate(terms: Array<string>, name: string): void;
    onRequestClose(): void;
}

interface DialogState {
    room: BingoRoom
    index: number;
    checked: boolean;
    templateName: string;
}
/**
 *
 */
export class BingoRoomCreationDialog extends React.Component<DialogProps, DialogState> {

    private templates: Array<Array<string>> = [
        [
            "Wipe to SE golems", "Staff Guard", "Pulling ALL the mobs", "No stealth at all", "Running out of SR in CM",
            "Scepter AA Ele", "Spawnin oozes in AC", "Banner of Tactics / Def", "Not talking English", "Flame thrower Engi",
            "Bubble bath at Lupi", "Greatsword Mesmer", "Random or no FB / WoR", "Death on CoF bridge", "Ranging the HotW troll",
            "Failing 1-5 in CoE", "Failing the first TA skip", "Shortbow Thief", "Rifle Warrior","Minion Necro",
            "Area retal \o/", "Bearbow Ranger", "OOC too hard to understand", "Bar full of MF food + buffs","Not answering in chat"
        ]
    ];

    constructor(props: DialogProps) {
        super(props);
        this.state = {
            room: BingoRoom.createDefault(),
            index: 0,
            checked: false,
            templateName: ""
        }
    }

    private setTemplateToDescriptions = (template: BingoTemplate) => {
        let bingoRoom: BingoRoom = this.state.room;
        bingoRoom = bingoRoom.terms(template.terms());
        this.setState({
            room: bingoRoom
        })
    };

    private changeDescription = (index: number, value: string) => {
        let terms = this.state.room.terms().set(index, value);
        this.setState({
            room: this.state.room.terms(terms)
        })
    };

    private changeRoomName = (event: any, value: string) => {
        this.setState({
            room: this.state.room.roomName(value)
        })
    };

    private changeRoomPassword = (event: any, value: string) => {
        this.setState({
            room: this.state.room.password(value)
        })
    };

    private progressStepper = () => {
        if(this.state.index == 0) {
            this.setState({
                index: 1
            });
        } else if(this.state.index == 1) {
            if(this.state.checked) {
                this.props.onCreateTemplate(this.state.room.terms().toArray(), this.state.templateName);
            }
            this.props.onCreate(this.state.room)
        }
    };

    private stepBack = () => {
        if (this.state.index >= 0) {
            this.setState({
                index: this.state.index - 1
            })
        }
    };

    private checkUncheckCheckBox = (event: object, isInputChecked: boolean) => {
        this.setState({
            checked: isInputChecked
        })
    };

    private renderEmptyFields = () => {
        let res: Array<JSX.Element> = [];
        for(let i = 0; i < 25; i++) {
            res.push(
                <GridTile key={"BingoRoomCreationDialog.Tile." + i}>
                    <TextField
                        floatingLabelText="Description"
                        onChange={(event, val) => this.changeDescription(i, val)}
                        value={this.state.room.terms().get(i)}
                    />
                </GridTile>
            )
        }
        return res;
    };

    private setTemplate = (val: string) => {
        let template = this.props.templates.filter(t => val == t.name()).first();
        this.setTemplateToDescriptions(template);
    };


    /**
     * <h2>Room Details</h2>

     <h2>Room Fields</h2>
     <SelectField floatingLabelText="Templates">
     <MenuItem onClick={() => this.setTemplateToDescriptions(0)} primaryText="Standard PUG 2017"/>
     </SelectField>
     <RaisedButton primary={true} label="Create" onClick={() => }/>
     * @returns {any}
     */

    private renderContent = () => {
        let res: JSX.Element = null;
        if(this.state.index == 0) {
            return (<div>
                <TextField
                    floatingLabelText="Room Name"
                    value={this.state.room.roomName()}
                    onChange={this.changeRoomName}
                />
                <TextField
                    floatingLabelText="Room Password"
                    value={this.state.room.password()}
                    onChange={this.changeRoomPassword}
                />
            </div>)
        } else if(this.state.index == 1) {
            return(<div>
                    <AutoComplete
                        floatingLabelText="Select Template"
                        dataSource={this.props.templates.map(t => t.name()).toArray()}
                        onNewRequest={this.setTemplate}
                        filter={AutoComplete.fuzzyFilter}
                    />
                    <Checkbox
                        checked={this.state.checked}
                        onCheck={this.checkUncheckCheckBox}
                        label="Save As Template"
                    />
                    {
                        this.state.checked
                            ?
                            <TextField
                                floatingLabelText="Template Name"
                                value={this.state.templateName}
                                onChange={(evt, val) => this.setState({templateName: val})}
                            />
                            :
                            null
                    }
                <div className="row">
                    <div className="col-md-12">
                        <GridList cols={5} cellHeight="auto">
                            {this.renderEmptyFields()}
                        </GridList>
                    </div>
                </div>
            </div>)
        }
        return null;
    };

    private getNextButtonLabel = () => {
        switch(this.state.index) {
            case 0: return "Select Terms";
            case 1: return "Finish";
        }
        return "";
    };

    public render(): JSX.Element {
        return (<div>
            <Dialog
                open={this.props.open}
                autoScrollBodyContent={true}
                onRequestClose={this.props.onRequestClose}
            >
                <Stepper activeStep={this.state.index}>
                    <Step>
                        <StepLabel>Properties</StepLabel>
                    </Step>
                    <Step>
                        <StepLabel>Terms</StepLabel>
                    </Step>
                </Stepper>
                {this.renderContent()}
                <br/>
                <RaisedButton style={{marginLeft: "20px"}} secondary={true} label="Prev" onClick={this.stepBack} disabled={this.state.index == 0}/>
                <RaisedButton style={{marginLeft: "20px"}} label={this.getNextButtonLabel()} onClick={this.progressStepper}/>
            </Dialog>
        </div>)
    }
}