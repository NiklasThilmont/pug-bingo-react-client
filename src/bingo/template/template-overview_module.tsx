import * as React from "react";
import {BingoTemplate} from "../../model/BingoTemplate";
import {Dialog, GridList, GridTile, Paper, Subheader} from "material-ui";
import {isNullOrUndefined} from "util";
import {cyan200} from "material-ui/styles/colors";
import {getMuiTheme, MuiTheme} from "material-ui/styles";
import {CSSProperties} from "react";

interface TemplateOverviewProps {
    template: BingoTemplate;
    open: boolean;
    onRequestClose(): void;
}

interface TemplateOverviewState {
    muiTheme: MuiTheme
}

export class TemplateOverview extends React.Component<TemplateOverviewProps, TemplateOverviewState> {

    constructor(props: TemplateOverviewProps) {
        super(props);
        this.state = {
            muiTheme: getMuiTheme()
        };
        this.tileStyles.backgroundColor = this.state.muiTheme.rawTheme.palette.primary1Color;
        this.tileStyles.color = this.state.muiTheme.rawTheme.palette.alternateTextColor;
    }

    private tileStyles: CSSProperties = {
        backgroundColor: "",
        color: ""
    };

    private renderTerm = (term: string) => {
        return (<GridTile
                cols={1}
                key={"TemplateOverview.Term." + term}
                style={this.tileStyles}
            >
                {term}
            </GridTile>)
    };

    private renderTerms = () => {
        let res: Array<JSX.Element> = [];
        this.props.template.terms().forEach(term => {
            res.push(this.renderTerm(term));
        });
        return res;
    };


    render() {
        if(!isNullOrUndefined(this.props.template)) {
            return (<Dialog
                open={this.props.open}
                onRequestClose={this.props.onRequestClose}
                title={"Details for " + this.props.template.name()}
            >
                <h3>Terms</h3>
                <br/>
                <GridList cols={5} cellHeight={90} style={{width: "550px", height:"550px"}}>
                    {this.renderTerms()}
                </GridList>
            </Dialog>)
        }
        return <div/>;
    }
}