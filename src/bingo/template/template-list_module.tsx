import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {ApplicationState} from "../../index";
import {BingoTemplate} from "../../model/BingoTemplate";
import * as Immutable from 'immutable';
import {FontIcon, GridList, GridTile, IconButton, Paper, Subheader} from "material-ui";
import {TemplateOverview} from "./template-overview_module";

/**
 * Properties that are managed by react-redux.
 *
 * Each property defined here will also need a corresponding mapping in the {@link TemplateList.mapStateToProps} method,
 * otherwise the component will not render and update correctly.
 */
interface TemplateListProps {
    templates: Immutable.List<BingoTemplate>;
}

/**
 * Local properties of this module.
 *
 * These properties are used to initialize the local state and to control everything that is solely used for the display layer.
 *
 * Data that is intended not only for display needs to be placed in the {@link TemplateListProps} and will then be
 * managed by redux.
 */
interface TemplateListLocalProps {

}

/**
 * Local state of the module.
 *
 * All display-only state fields, such as bool flags that define if an element is visibile or not, belong here.
 */
interface TemplateListLocalState {
    selectedTemplate: BingoTemplate;
    dialogOpen: boolean;
}

/**
 * Defines mappings from local handlers to redux dispatches that invoke actions on the store.
 */
interface TemplateListDispatch {

}

class TemplateListModule extends React.Component<TemplateListProps & TemplateListProps & TemplateListDispatch, TemplateListLocalState> {

    constructor(props: TemplateListProps & TemplateListProps & TemplateListDispatch) {
        super(props);
        this.state = {
            selectedTemplate: null,
            dialogOpen: false
        }
    }

    static mapStateToProps(state: ApplicationState, localProps: TemplateListProps): TemplateListProps {
        return {
            templates: state.bingoStore.templates()
        }
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<ApplicationState>): TemplateListDispatch {
        return {}
    }

    private style = {
        margin: 20,
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 10,
        paddingBottom: 10,
        textAlign: 'center !important',
        display: 'inline-block',
    };

    private openTemplate = (index: number) => {
        this.setState({
            selectedTemplate: this.props.templates.get(index),
            dialogOpen: true
        })
    };

    private closeTemplate = () => {
        this.setState({
            selectedTemplate: null,
            dialogOpen: false
        })
    };


    private renderTiles = () => {
        return this.props.templates.map((template, index) => {
            return(<Paper
                key={"Template.GridTile." + template.id()}
                zDepth={1}
                style={this.style}
                onClick={() => this.openTemplate(index)}
            >
                <h4>{template.name()}</h4>
            </Paper>)
        }).toArray();

    };

    render() {
        return (<div>
            <TemplateOverview
                template={this.state.selectedTemplate}
                open={this.state.dialogOpen}
                onRequestClose={this.closeTemplate}
            />
            {this.renderTiles()}
            <Paper
                key={"Template.GridTile.New"}
                style={this.style}
                onClick={() => {alert("Not yet implemented.")}}
            >
                <IconButton
                    iconClassName="material-icons"
                    tooltip="New Template"
                >
                    create
                </IconButton>
            </Paper>
        </div>);
    }
}

/**
 * @see TemplateListModule
 * @author Niklas
 * @since 04.06.2017
 */
export const TemplateList: React.ComponentClass<TemplateListLocalProps> = connect(TemplateListModule.mapStateToProps, TemplateListModule.mapDispatchToProps)(TemplateListModule);