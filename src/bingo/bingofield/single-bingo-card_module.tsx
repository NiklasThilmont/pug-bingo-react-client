import {connect} from "react-redux";
import * as React from "react";
import * as redux from "redux";
import {Paper} from "material-ui";
import {ApplicationState} from "../../index";
import {BingoField} from "../../model/BingoField";
import CSSProperties = React.CSSProperties;

/**
 * Properties that are managed by react-redux.
 *
 * Each property defined here will also need a corresponding mapping in the {@link BingoCard.mapStateToProps} method,
 * otherwise the component will not render and update correctly.
 */
interface BingoCardProps {

}

/**
 * Local properties of this module.
 *
 * These properties are used to initialize the local state and to control everything that is solely used for the display layer.
 *
 * Data that is intended not only for display needs to be placed in the {@link BingoCardProps} and will then be
 * managed by redux.
 */
interface BingoCardLocalProps {
    field: BingoField;
}

/**
 * Local state of the module.
 *
 * All display-only state fields, such as bool flags that define if an element is visibile or not, belong here.
 */
interface BingoCardLocalState {
}

/**
 * Defines mappings from local handlers to redux dispatches that invoke actions on the store.
 */
interface BingoCardDispatch {
    bingoIt(index: number): void;
}

class BingoCardModule extends React.Component<BingoCardProps & BingoCardLocalProps & BingoCardDispatch, BingoCardLocalState> {


    private cardStyle: CSSProperties = {
        height: "100px",
        width: "100px"
    };

    constructor(props: BingoCardProps & BingoCardLocalProps & BingoCardDispatch) {
        super(props);

    }

    static mapStateToProps(state: ApplicationState, localProps: BingoCardLocalProps): BingoCardProps {
        return {
        }
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<ApplicationState>): BingoCardDispatch {
        return {
            bingoIt: function(index: number) {

            }
        }
    }

    render() {
        return (<Paper
            className={this.props.field.bingoed() ? "redBorder" : "noBorder"}
            style={this.cardStyle}
        >
            {this.props.field.description()}
        </Paper>);
    }
}

/**
 * @see BingoCardModule
 * @author Niklas
 * @since 25.05.2017
 */
export const BingoCard: React.ComponentClass<BingoCardLocalProps> = connect(BingoCardModule.mapStateToProps, BingoCardModule.mapDispatchToProps)(BingoCardModule);