import {connect} from "react-redux";
import * as React from "react";
import * as redux from "redux";
import {GridList} from "material-ui";
import {BingoCard} from "./single-bingo-card_module";
import {ApplicationState} from "../../index";
import {BingoField} from "../../model/BingoField";
import {BingoBoard} from "../../model/BingoBoard";

/**
 * Properties that are managed by react-redux.
 *
 * Each property defined here will also need a corresponding mapping in the {@link BingoField.mapStateToProps} method,
 * otherwise the component will not render and update correctly.
 */
interface BingoFieldProps {

}

/**
 * Local properties of this module.
 *
 * These properties are used to initialize the local state and to control everything that is solely used for the display layer.
 *
 * Data that is intended not only for display needs to be placed in the {@link BingoFieldProps} and will then be
 * managed by redux.
 */
interface BingoFieldLocalProps {
    field: BingoBoard;
}

/**
 * Local state of the module.
 *
 * All display-only state fields, such as bool flags that define if an element is visibile or not, belong here.
 */
interface BingoFieldLocalState {

}

/**
 * Defines mappings from local handlers to redux dispatches that invoke actions on the store.
 */
interface BingoFieldDispatch {

}

class BingoFieldModule extends React.Component<BingoFieldProps & BingoFieldLocalProps & BingoFieldDispatch, BingoFieldLocalState> {

    static mapStateToProps(state: ApplicationState, localProps: BingoFieldLocalProps): BingoFieldProps {
        return {
        }
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<ApplicationState>): BingoFieldDispatch {
        return {}
    }

    private renderCards = () => {
        let res: Array<JSX.Element> = [];
        console.log(this.props);
        this.props.field.fieldList().forEach((value: BingoField, key: number) => {
            res.push(<BingoCard key={"BingoBoard.Field." + key} field={value} />)
        });
        return res;
    };

    render() {
        return (
            <GridList
                cols={5}
                cellHeight="auto"
                style={{width: "520px"}}
            >
                {this.renderCards()}
            </GridList>
        )
    }
}

/**
 * @see BingoFieldModule
 * @author Niklas
 * @since 25.05.2017
 */
export const BingoGameField: React.ComponentClass<BingoFieldLocalProps> = connect(BingoFieldModule.mapStateToProps, BingoFieldModule.mapDispatchToProps)(BingoFieldModule);