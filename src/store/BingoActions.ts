import {BingoUser} from "../model/BingoUser";
import {BingoRoomInfo} from "../model/BingoRoomInfo";
import {BingoRoom} from "../model/BingoRoom";
import {BingoTemplate} from "../model/BingoTemplate";
import {RouteTarget} from "../model/RouteTarget";

export enum ActionType {
    SetUser,
    AddRoomInfo,
    RemoveRoom,
    RemoveRoomInfo,
    RequestJoinRoom,
    RejectJoinRoom,
    AcceptJoinRoom,
    StopJoinRoom,
    LoadRoom,
    UpdateTimer,
    AddTemplate,
    ClearTemplates,
    ChangeRoute
}

export interface AbstractAction {
    type: ActionType;
}


export interface AddRoomInfoAction extends AbstractAction {
    roomInfo: BingoRoomInfo
}

export interface SetUserAction extends AbstractAction {
    user: BingoUser
}

/**
 * Loads the room into the selected room.
 */
export interface LoadRoomAction extends AbstractAction {
    room: BingoRoom;
}

export interface RemoveRoomAction extends AbstractAction {
    roomId: string;
}

export interface UpdateTimerAction extends AbstractAction {
    timer: NodeJS.Timer;
}

export interface AddTemplateAction extends AbstractAction {
    template: BingoTemplate;
}

export interface ChangeRouteAction extends AbstractAction {
    route: RouteTarget;
}