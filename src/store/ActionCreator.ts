import {
    AbstractAction,
    ActionType,
    AddRoomInfoAction, AddTemplateAction, ChangeRouteAction,
    LoadRoomAction,
    RemoveRoomAction,
    SetUserAction,
    UpdateTimerAction
} from "./BingoActions";
import * as redux from "redux";
import {BingoRoom} from "../model/BingoRoom";
import {BingoUser} from "../model/BingoUser";
import {ApplicationState, browserHistory} from "../index";
import axios, {AxiosError, AxiosRequestConfig, AxiosResponse} from "axios";
import {
    deleteLeaveRoomAPIString,
    getAllRoomsAPIString, getAllTemplateIdsString,
    getDeleteRoomAPIString,
    getLogInUserAPIString,
    getPostCallTermAPIString,
    getPostRoomAPIString,
    getPostUserAPIString,
    getRoomAPIString,
    getRoomInfoAPIString, getTemplateString,
    postJoinRoomAPIString, postNewTemplateString
} from "../APIConfig";
import {BingoRoomInfo} from "../model/BingoRoomInfo";
import {APIRoomInfo} from "../model/api/APIRoomInfo";
import {isNullOrUndefined} from "util";
import {APIBingoUser} from "../model/api/APIBingoUser";
import {APITemplate} from "../model/api/APITemplate";
import {BingoTemplate} from "../model/BingoTemplate";
import {RouteTarget} from "../model/RouteTarget";
import {Cookie} from "../model/cookie/Cookie";

export class ActionCreator {

    public static AddRoomInfo(roomInfo: BingoRoomInfo): AddRoomInfoAction {
        return {
            type: ActionType.AddRoomInfo,
            roomInfo: roomInfo
        }
    }

    public static SetUser(user: BingoUser): SetUserAction {
        return {
            type: ActionType.SetUser,
            user: user
        }
    }

    public static RequestJoinRoom(): AbstractAction {
        return {
            type: ActionType.RequestJoinRoom
        }
    }

    public static RejectJoinRoom(): AbstractAction {
        return {
            type: ActionType.RejectJoinRoom
        }
    }

    public static LoadRoom(room: BingoRoom): LoadRoomAction {
        return {
            type: ActionType.LoadRoom,
            room: room
        }
    }

    public static AcceptJoinRoom(): AbstractAction {
        return {
            type: ActionType.AcceptJoinRoom
        }
    }

    public static StopJoinRoom() : AbstractAction {
        return {
            type: ActionType.StopJoinRoom
        }
    }

    public static RemoveRoom(roomId: string): RemoveRoomAction {
        return {
            type: ActionType.RemoveRoom,
            roomId: roomId
        }
    }

    public static UpdateTimer(timer: NodeJS.Timer): UpdateTimerAction {
        return {
            type: ActionType.UpdateTimer,
            timer: timer
        }
    }

    public static AddTemplate(template: BingoTemplate): AddTemplateAction {
        return {
            type: ActionType.AddTemplate,
            template: template
        }
    }

    public static ClearTemplates(): AbstractAction {
        return {
            type: ActionType.ClearTemplates
        }
    }

    public static ChangeRoute(target: RouteTarget): ChangeRouteAction {
        return {
            type: ActionType.ChangeRoute,
            route: target
        }
    }

    public static RemoveRoomInfo(id: string): RemoveRoomAction {
        return {
            type: ActionType.RemoveRoomInfo,
            roomId: id
        }
    }

    /**
     * Invokes a dispatch for a delta operation on the current room list the API serves and the room list
     * available at this client.
     * This will remove all rooms not available with the API anymore and will add rooms that have been added
     * Note that this is an expensive operation. It should not be invoked manually.
     * @returns {(dispatch:redux.Dispatch<ApplicationState>, getState:()=>ApplicationState)=>undefined}
     * @constructor
     */
    public static AsyncReloadMissingRooms() {
        return function (dispatch: redux.Dispatch<ApplicationState>, getState: () => ApplicationState) {
            axios.get(getAllRoomsAPIString()).then((response: AxiosResponse) => {
                let rooms = getState().bingoStore.roomInfos();
                let roomIds: Set<string> = new Set<string>(<Array<string>>response.data);
                rooms.forEach((room: BingoRoomInfo, roomId: string) => {
                    if(roomIds.has(roomId)) {
                        roomIds.delete(roomId);
                    } else {
                        dispatch(ActionCreator.RemoveRoomInfo(roomId))
                    }
                });
                roomIds.forEach(id => dispatch(ActionCreator.AsyncGetRoomForList(id)));
            }).catch(error => {
                console.error(error);
            })
        }
    }

    public static AsyncGetRoomForList(roomId: string) {
        return function (dispatch: redux.Dispatch<ApplicationState>) {
            axios.get(getRoomInfoAPIString(roomId)).then((response: AxiosResponse) => {
                let apiInfo: APIRoomInfo = response.data;
                let bingoRoomInfo: BingoRoomInfo = BingoRoomInfo.fromAPI(apiInfo);
                dispatch(ActionCreator.AddRoomInfo(bingoRoomInfo));
            }).catch(error => {
                console.error(error);
            })
        }
    }

    public static AsyncCreateRoom(room: BingoRoom) {
        return function (dispatch: redux.Dispatch<ApplicationState>) {
            axios.post(getPostRoomAPIString(), room.toAPICreateRoom()).then((response: AxiosResponse) => {
                let room: BingoRoom = BingoRoom.fromAPI(response.data);
                dispatch(ActionCreator.AsyncGetRoomForList(room.id()));
            }).catch(error => {
                console.error(error);
            })
        }
    }

    public static AsyncJoinRoom(roomId: string, userId: string, password?: string) {
        return function (dispatch: redux.Dispatch<ApplicationState>) {
            let config = null;
            if(!isNullOrUndefined(password)) config = {headers: {password: password}};
            axios.post(postJoinRoomAPIString(roomId, userId),undefined, config).then((response: AxiosResponse) => {
                let room: BingoRoom = BingoRoom.fromAPI(response.data);
                dispatch(ActionCreator.AcceptJoinRoom());
                dispatch(ActionCreator.LoadRoom(room));
                dispatch(ActionCreator.ChangeRoute(RouteTarget.Room));
                dispatch(ActionCreator.AsyncStartRefreshRoom());
            }).catch((err: AxiosError) => {
                if(err.response.status == 403) {
                    dispatch(ActionCreator.RejectJoinRoom());
                }
                console.error(err);
            })
        }
    }

    public static AsyncLeaveRoom(roomId: string, userId: string, password?: string) {
        return function (dispatch: redux.Dispatch<ApplicationState>) {
            let config = null;
            if(!isNullOrUndefined(password)) config = {headers: {password: password}};
            axios.delete(deleteLeaveRoomAPIString(roomId, userId), config).then((response: AxiosResponse) => {
                dispatch(ActionCreator.ChangeRoute(RouteTarget.RoomList));
                dispatch(ActionCreator.AsyncStopRefreshRoom());
            }).catch((err: AxiosError) => {
                console.error(err);
            })
        }
    }

    public static AsyncCallTerm(roomId: string, term: string, roomPass?: string) {
        return function (dispatch: redux.Dispatch<ApplicationState>) {
            let config: AxiosRequestConfig = null;
            if(!isNullOrUndefined(roomPass)) config = {headers: {password: roomPass}};
            let body = { term: term };
            axios.post(getPostCallTermAPIString(roomId), body, config).then((response: AxiosResponse) => {
                let room: BingoRoom = BingoRoom.fromAPI(response.data);
                dispatch(ActionCreator.LoadRoom(room));
            }).catch((err: AxiosError) => {
                console.error(err);
            })
        }
    }

    public static AsyncCreateOrLogIn(username: string) {
        return function (dispatch: redux.Dispatch<ApplicationState>) {
            let user: APIBingoUser = {
                id: null,
                key: null,
                name: username
            };
            axios.post(getPostUserAPIString(), user).then((response: AxiosResponse) => {
                let user: BingoUser = BingoUser.fromAPI(response.data);
                dispatch(ActionCreator.SetUser(user));
                dispatch(ActionCreator.AsyncWriteUsernameToCookie(user.name()))
            }).catch((err: AxiosError) => {
                console.error(err);
            })
        }
    }

    public static AsyncDeleteRoom(roomId: string, password?: string) {
        return function (dispatch: redux.Dispatch<ApplicationState>) {
            let config: AxiosRequestConfig = null;
            if(!isNullOrUndefined(password)) config = {headers: {password: password}};
            axios.delete(getDeleteRoomAPIString(roomId), config).then((response: AxiosResponse) => {
                dispatch(ActionCreator.RemoveRoom(roomId));
                dispatch(ActionCreator.AsyncStopRefreshRoom());
            }).catch((err: AxiosError) => {
                console.error(err);
            })
        }
    }

    private static AsyncRefreshRoom() {
        return function (dispatch: redux.Dispatch<ApplicationState>, getState: () => ApplicationState) {
            let store = getState().bingoStore;
            let roomId = store.currentRoom().id();
            let password = store.currentRoom().password();
            let config: AxiosRequestConfig = null;
            if(!isNullOrUndefined(password)) config = {headers: {password: password}};
            axios.get(getRoomAPIString(roomId), config).then((response: AxiosResponse) => {
                let room: BingoRoom = BingoRoom.fromAPI(response.data);
                dispatch(ActionCreator.LoadRoom(room));
                let timer: any = setTimeout(() => dispatch(ActionCreator.AsyncRefreshRoom()), store.refreshInterval());
                dispatch(ActionCreator.UpdateTimer(timer));
            }).catch((err: AxiosError) => {
                console.error(err);
            });
        }
    }

    public static AsyncStopRefreshRoom() {
        return function (dispatch: redux.Dispatch<ApplicationState>, getState: () => ApplicationState) {
            let store = getState().bingoStore;
            clearTimeout(store.refreshIntervalTimer());
        }
    }

    public static AsyncStartRefreshRoom() {
        return function (dispatch: redux.Dispatch<ApplicationState>) {
            dispatch(ActionCreator.AsyncRefreshRoom());
        }
    }

    public static AsyncLoadTemplate(id: string) {
        return function (dispatch: redux.Dispatch<ApplicationState>) {
            axios.get(getTemplateString(id)).then((response: AxiosResponse) => {
                let apiTemplate: APITemplate = response.data;
                let template: BingoTemplate = BingoTemplate.fromAPI(apiTemplate);
                dispatch(ActionCreator.AddTemplate(template));
            }).catch((err: AxiosError) => {
                console.error(err);
            });
        }
    }

    public static AsyncLoadAllTemplates() {
        return function (dispatch: redux.Dispatch<ApplicationState>) {
            dispatch(ActionCreator.ClearTemplates());
            axios.get(getAllTemplateIdsString()).then((response: AxiosResponse) => {
                let templateIds: Array<string> = response.data;
                templateIds.forEach(id => {
                    dispatch(ActionCreator.AsyncLoadTemplate(id));
                })
            }).catch((err: AxiosError) => {
                console.error(err);
            });
        }
    }

    public static AsyncSaveTemplate(terms: Array<string>, name: string) {
        return function (dispatch: redux.Dispatch<ApplicationState>) {
            let apiTemplate: APITemplate = {
                name: name,
                terms: terms,
                id: null
            };
            axios.post(postNewTemplateString(), apiTemplate).then((response: AxiosResponse) => {
                console.log("Template posted");
            }).catch((err: AxiosError) => {
                console.error(err);
            });
        }
    }

    public static AsyncNavigateTo(to: string) {
        return function (dispatch: redux.Dispatch<ApplicationState>) {
            browserHistory.push(to);
        }
    }

    public static AsyncWriteUsernameToCookie(username: string) {
        return function (dispatch: redux.Dispatch<ApplicationState>) {
            Cookie.username = username;
        }
    }

    public static AsyncLoadUserFromCookie() {
        return function (dispatch: redux.Dispatch<ApplicationState>) {
            let username = Cookie.username;
            if(!isNullOrUndefined(username)) {
                dispatch(ActionCreator.AsyncCreateOrLogIn(username));
            }
        }
    }

    public static AsyncLogOutUser() {
        return function (dispatch: redux.Dispatch<ApplicationState>) {
            Cookie.username = null;
            dispatch(ActionCreator.SetUser(null));
        }
    }



}