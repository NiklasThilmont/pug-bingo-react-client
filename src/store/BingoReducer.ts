import {BingoStore} from "../model/BingoStore";
import {isNullOrUndefined} from "util";
import {
    AbstractAction,
    ActionType,
    AddRoomInfoAction, AddTemplateAction, ChangeRouteAction,
    LoadRoomAction,
    RemoveRoomAction,
    SetUserAction, UpdateTimerAction
} from "./BingoActions";
import {RoomEnterStatus} from "../model/RoomEnterStatus";


export class BingoReducer {
    public static AddRoomInfo(store: BingoStore, action: AddRoomInfoAction): BingoStore {
        return store.roomInfos(store.roomInfos().set(action.roomInfo.id(), action.roomInfo));
    }

    public static LoadRoom(store: BingoStore, action: LoadRoomAction): BingoStore {
        return store.currentRoom(action.room);
    }

    public static SetUser(store: BingoStore, action: SetUserAction): BingoStore {
        return store.user(action.user);
    }

    public static RemoveRoom(store: BingoStore, action: RemoveRoomAction): BingoStore {
        if(store.currentRoom().id() == action.roomId) {
            store = store.currentRoom(null);
        }
        return store.roomInfos(store.roomInfos().remove(action.roomId));
    }

    public static UpdateTimer(store: BingoStore, action: UpdateTimerAction): BingoStore {
        return store.refreshIntervalTimer(action.timer);
    }

    public static AddTemplate(store: BingoStore, action: AddTemplateAction): BingoStore {
        return store.templates(store.templates().push(action.template));
    }

    public static RemoveRoomInfo(store: BingoStore, action: RemoveRoomAction): BingoStore {
        return store.roomInfos(store.roomInfos().remove(action.roomId));
    }

}

export function bingoStore(store: BingoStore, action: AbstractAction) : BingoStore {
    console.log("Bingo Reducer called with " + ActionType[action.type]);
    if(isNullOrUndefined(store)) {
        return BingoStore.createEmpty();
    }
    switch (action.type) {
        case ActionType.AddRoomInfo:
            return BingoReducer.AddRoomInfo(store, action as AddRoomInfoAction)
        case ActionType.RequestJoinRoom:
            return store.roomEnterStatus(RoomEnterStatus.RequestPassword);
        case ActionType.RejectJoinRoom:
            return store.roomEnterStatus(RoomEnterStatus.PasswordReject);
        case ActionType.AcceptJoinRoom:
        case ActionType.StopJoinRoom:
            return store.roomEnterStatus(RoomEnterStatus.None);
        case ActionType.LoadRoom:
            return BingoReducer.LoadRoom(store, action as LoadRoomAction);
        case ActionType.SetUser:
            return BingoReducer.SetUser(store, action as SetUserAction);
        case ActionType.RemoveRoom:
            return BingoReducer.RemoveRoom(store, action as RemoveRoomAction);
        case ActionType.UpdateTimer:
            return BingoReducer.UpdateTimer(store, action as UpdateTimerAction);
        case ActionType.AddTemplate:
            return  BingoReducer.AddTemplate(store, action as AddTemplateAction);
        case ActionType.ClearTemplates:
            return store.templates(store.templates().clear());
        case ActionType.ChangeRoute:
            return store.routeTarget((action as ChangeRouteAction).route);
        case ActionType.RemoveRoomInfo:
            return BingoReducer.RemoveRoomInfo(store, action as RemoveRoomAction);
    }
    return store;
}