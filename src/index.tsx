import * as React from "react";
import * as ReactDOM from "react-dom";
import thunkMiddleware from "redux-thunk";
import registerServiceWorker from "./registerServiceWorker";
import "./index.css";
import {Provider} from "react-redux";
import {applyMiddleware, combineReducers, createStore, Store} from "redux";
import {bingoStore} from "./store/BingoReducer";
import {BingoStore} from "./model/BingoStore";
import {ActionCreator} from "./store/ActionCreator";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import lightBaseTheme from "material-ui/styles/baseThemes/lightBaseTheme";
import getMuiTheme from "material-ui/styles/getMuiTheme";
import {BingoApp} from "./bingo/bingo-app_module";
import {BingoRoomList} from "./bingo/room/bingo-room-list_module";
import {SingleBingoRoom} from "./bingo/room/single-bingo-room_module";
import {AccountInfo} from "./bingo/account/account-info_module";
import {Router} from "react-router-dom";
import {MemoryRouter, Route, StaticRouter} from "react-router";
import injectTapEventPlugin = require("react-tap-event-plugin");
import createBrowserHistory from "history/createBrowserHistory";


injectTapEventPlugin();


// Build the middleware for intercepting and dispatching navigation actions

export interface ApplicationState {
    bingoStore: BingoStore
}

const rootReducer = combineReducers({
    bingoStore,
});

const store: Store<any> = createStore(
    rootReducer,
    applyMiddleware(
        thunkMiddleware
    )
);

export const browserHistory = createBrowserHistory();

store.dispatch(ActionCreator.AsyncReloadMissingRooms());
store.dispatch(ActionCreator.AsyncLoadAllTemplates());
store.dispatch(ActionCreator.AsyncLoadUserFromCookie());


ReactDOM.render(
    <Provider store={store}>
        <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
                <BingoApp/>
        </MuiThemeProvider>
    </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
