import {doop} from "doop";
import * as Immutable from 'immutable';
import {APIBingoUser} from "./api/APIBingoUser";
import {APITemplate} from "./api/APITemplate";

@doop
export class BingoTemplate {
    @doop
    public get id() {return doop<string, this>()}

    @doop
    public get name() {return doop<string, this>()};

    @doop
    public get terms() {return doop<Immutable.List<string>, this>()};

    public constructor(id: string, name: string, terms: Immutable.List<string>) {
        this.id(id).name(name).terms(terms);
    }


    public static fromAPI(apiBingoTemplate: APITemplate) {
        return new BingoTemplate(apiBingoTemplate.id, apiBingoTemplate.name, Immutable.List<string>(apiBingoTemplate.terms));
    }
}