export interface APITemplate {
    id: string;
    name: string;
    terms: Array<string>;
}