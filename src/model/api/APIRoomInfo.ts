export interface APIRoomInfo {
    id: string;
    roomName: string;
    hasPassword: boolean;
}