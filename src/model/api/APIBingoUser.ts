export interface APIBingoUser {
    id: string;
    name: string;
    key: string;
}