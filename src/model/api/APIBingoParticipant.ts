import {APIBingoBoard} from "./APIBingoBoard";
export interface APIBingoParticipant {
    bingoBoard: APIBingoBoard;
    bingoUserId: string;
    bingoUserName: string;
}