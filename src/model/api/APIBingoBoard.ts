import {APIBingoField} from "./APIBingoField";
export interface APIBingoBoard {
    fieldList: Array<APIBingoField>;
    isBingo: boolean;
}