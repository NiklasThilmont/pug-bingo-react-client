import {APIBingoParticipant} from "./APIBingoParticipant";
export interface APIBingoRoom {
    id: string;
    roomName: string;
    password: string;
    terms: Array<string>;
    participantList: Array<APIBingoParticipant>;
    winnerIds: Array<string>;
}