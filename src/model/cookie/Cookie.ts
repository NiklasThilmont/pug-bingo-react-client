import {isNullOrUndefined} from "util";
interface ICookie {
    username: string;
}

export class Cookie {

    private static readonly cookie_name_username = "username";

    public static get username() {
        let username: string = this.getCookie(this.cookie_name_username);
        return username == "" ? null : username;
    };

    public static set username(username: string) {
        this.setCookie(this.cookie_name_username, username, 1);
    };

    private static setCookie(cname: string, cvalue: string, exdays: number) {
        let d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        let expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    private static getCookie(cname: string) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for(let i = 0; i <ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    constructor() {

    }
}