import {doop} from "doop";
import {APIBingoUser} from "./api/APIBingoUser";

@doop
export class BingoUser {

    @doop
    public get id() {
        return doop<string, this>();
    }

    @doop
    public get name() {
        return doop<string, this>();
    }

    public static currentId: number = 0;


    private constructor(id: string, name: string) {
        return this.id(id).name(name);
    }

    public static construct(id: string, name: string) {
        return new BingoUser(id, name);
    }

    /**
     * Creates an empty, non functional user.
     */
    public static createEmpty() {
        return new BingoUser(null, "");
    }

    public static createAnon() {
        return new BingoUser(String(++BingoUser.currentId) + "__NEW", "Anon" + BingoUser.currentId);
    }

    public static fromAPI(apiUser: APIBingoUser) {
        return new BingoUser(apiUser.id, apiUser.name);
    }

}