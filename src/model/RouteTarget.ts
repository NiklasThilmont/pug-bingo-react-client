export enum RouteTarget {
    RoomList,
    Room,
    Account,
    Templates
}