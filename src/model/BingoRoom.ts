import {doop} from "doop";
import * as Immutable from "immutable";
import {BingoParticipant} from "./BingoParticipant";
import {APIBingoRoom} from "./api/APIBingoRoom";

@doop
export class BingoRoom {

    @doop
    public get id(){
        return doop<string, this>();
    }

    @doop
    public get roomName() {
        return doop<string, this>();
    }

    /**
     * Room password. may be null.
     * @returns {Doop<string, this>}
     */
    @doop
    public get password() {
        return doop<string, this>();
    }

    @doop
    public get terms() {
        return doop<Immutable.List<string>, this>();
    }

    @doop
    public get participants() {
        return doop<Immutable.List<BingoParticipant>, this>();
    }

    @doop
    public get winnerIds() {
        return doop<Immutable.List<string>, this>();
    }


    private constructor(id: string, roomName: string, password: string, terms: Immutable.List<string>,
                        participants: Immutable.List<BingoParticipant>, winnerIds: Immutable.List<string>) {
        return this.id(id).roomName(roomName).password(password).terms(terms).participants(participants).winnerIds(winnerIds);
    }

    public static fromAPI(apiBingoRoom: APIBingoRoom): BingoRoom {
        return new BingoRoom(
            apiBingoRoom.id,
            apiBingoRoom.roomName,
            apiBingoRoom.password,
            Immutable.List<string>(apiBingoRoom.terms),
            Immutable.List<BingoParticipant>(apiBingoRoom.participantList.map(participant => BingoParticipant.fromAPI(participant))),
            Immutable.List<string>(apiBingoRoom.winnerIds)
        )
    }

    public toAPICreateRoom() : APIBingoRoom {
        return {
            id: null,
            participantList: null,
            roomName: this.roomName(),
            terms: this.terms().toArray(),
            password: this.password(),
            winnerIds: this.winnerIds().toArray()
        }
    }

    public static createDefault(): BingoRoom {
        return new BingoRoom(null,
            "",
            "",
            Immutable.List<string>(),
            Immutable.List<BingoParticipant>(),
            Immutable.List<string>());
    }

}