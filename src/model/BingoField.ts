import {doop} from "doop";
import {APIBingoField} from "./api/APIBingoField";
@doop
export class BingoField {

    @doop
    public get bingoed() {
        return doop<boolean, this>();
    }

    @doop
    public get description() {
        return doop<string, this>();
    }

    private constructor(bingoed: boolean, description: string) {
        return this.bingoed(bingoed).description(description);
    }

    public static create(description: string): BingoField {
        return new BingoField(false, description);
    }

    public static fromAPI(apiBingoField: APIBingoField): BingoField {
        return new BingoField(apiBingoField.called, apiBingoField.description);
    }

}