import {doop} from "doop";
import * as Immutable from "immutable";
import {BingoUser} from "./BingoUser";
import {RoomEnterStatus} from "./RoomEnterStatus";
import {BingoRoomInfo} from "./BingoRoomInfo";
import {BingoRoom} from "./BingoRoom";
import {BingoTemplate} from "./BingoTemplate";
import {RouteTarget} from "./RouteTarget";

export const NO_VALUE_ID = -1;

@doop
export class BingoStore {

    @doop
    public get id() {
        return doop<number, this>();
    }

    @doop
    public get currentRoom() {
        return doop<BingoRoom, this>();
    }

    /**
     * The user that is logged in.
     */
    @doop
    public get user() {
        return doop<BingoUser, this>();
    }

    @doop
    public get roomInfos() {
        return doop<Immutable.Map<string, BingoRoomInfo>, this>();
    }

    @doop
    public get roomEnterStatus() {
        return doop<RoomEnterStatus, this>();
    }

    @doop
    public get refreshInterval() {
        return doop<number, this>();
    }

    @doop
    public get refreshIntervalTimer() {
        return doop<NodeJS.Timer, this>();
    }

    @doop
    public get templates() {
        return doop<Immutable.List<BingoTemplate>, this>();
    }

    @doop
    public get routeTarget() {
        return doop<RouteTarget, this>();
    }

    private static CURRENT_ID: number = 0;

    constructor(id: number,
                rooms: Immutable.Map<string, BingoRoomInfo>,
                user: BingoUser,
                bingoRoom: BingoRoom,
                roomEnterStatus: RoomEnterStatus,
                templates: Immutable.List<BingoTemplate>,
                routeTarget: RouteTarget) {
        return this.id(id)
            .roomInfos(rooms)
            .user(user)
            .currentRoom(bingoRoom)
            .roomEnterStatus(roomEnterStatus)
            .refreshInterval(3000)
            .refreshIntervalTimer(null)
            .templates(templates)
            .routeTarget(routeTarget);
    }


    public static createEmpty() {
        return new BingoStore(++this.CURRENT_ID, Immutable.Map<string, BingoRoomInfo>(), null, null,
            RoomEnterStatus.None, Immutable.List<BingoTemplate>(), RouteTarget.RoomList);
    }
}