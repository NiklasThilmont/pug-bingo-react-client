export enum RoomEnterStatus {
    None,
    RequestPassword,
    PasswordReject
}