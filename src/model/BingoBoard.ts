import {doop} from "doop";
import {BingoField} from "./BingoField";
import * as Immutable from "immutable";
import {APIBingoBoard} from "./api/APIBingoBoard";

@doop
export class BingoBoard {

    @doop
    public get fieldList() {
        return doop<Immutable.List<BingoField>, this>();
    }

    @doop
    public get isBingo() {
        return doop<boolean, this>();
    }

    private constructor(fieldList: Immutable.List<BingoField>, isBingo: boolean) {
        return this.fieldList(fieldList).isBingo(isBingo);
    }

    public static createEmpty() {
        return new BingoBoard(Immutable.List<BingoField>(), false);
    }

    public static fromAPI(apiBingoBoard: APIBingoBoard) {
        let list = Immutable.List<BingoField>(apiBingoBoard.fieldList.map(field => BingoField.fromAPI(field)));
        return new BingoBoard(list, apiBingoBoard.isBingo);
    }
}