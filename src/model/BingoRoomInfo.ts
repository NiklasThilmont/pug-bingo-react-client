
import {doop} from "doop";
import {APIRoomInfo} from "./api/APIRoomInfo";
@doop
export class BingoRoomInfo {
    @doop
    public get id() {return doop<string, this>()};

    @doop
    public get roomName() {return doop<string, this>()};

    @doop
    public get hasPassword() {return doop<boolean, this>()};

    private constructor(id: string, name: string, hasPassword: boolean) {
        return this.id(id).roomName(name).hasPassword(hasPassword);
    }

    public static fromAPI(apiBingoRoomInfo: APIRoomInfo) {
        return new BingoRoomInfo(apiBingoRoomInfo.id, apiBingoRoomInfo.roomName, apiBingoRoomInfo.hasPassword);
    }
}