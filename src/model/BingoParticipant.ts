import {doop} from "doop";
import {BingoBoard} from "./BingoBoard";
import {APIBingoParticipant} from "./api/APIBingoParticipant";


@doop
export class BingoParticipant {

    @doop
    public get userId() {
        return doop<string, this>();
    }

    @doop
    public get userName() {
        return doop<string, this>();
    }

    @doop
    public get bingoBoard() {
        return doop<BingoBoard, this>();
    }

    private constructor(bingoBoard: BingoBoard, userId: string, userName: string) {
        return this.bingoBoard(bingoBoard).userId(userId).userName(userName);
    }

    public static fromAPI(apiBingoParticipant: APIBingoParticipant) {
        return new BingoParticipant(
            BingoBoard.fromAPI(
                apiBingoParticipant.bingoBoard), apiBingoParticipant.bingoUserId, apiBingoParticipant.bingoUserName)
    }
}